import React, { Component } from 'react';
// import {getItems} from '../services/items';
import {Link} from "react-router-dom"

class Items extends Component {
    state = { 
        items:[]
     }
    componentDidMount() {

      //  this.setState({items:getItems()})
      
      fetch('http://local.engageapi:8080/api/getItems')
        .then(res => res.json())
        .then((response) => {
          this.setState({ items: response })
        })
        
      }
     
    
    
    render() { 
        return ( 
        <main>
        <div className="main">
        <h2>Superior Quality</h2>
    
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, seder do eiusmod
          tempor incididunt ui labor et dolor magna aliqua. Cum tryi sociis natoque
          penatibus et. Lectus magna fringilla urna porttitor ur placo in egestas
          erata asdasd imperdiet .
        </p>
    
        <div className="items">
          {this.state.items.map(item=>
           <Link className="link" to ={`items/${item.id}`} key={item.id}>
         <div style={{backgroundImage:"url("+item.image+")",backgroundSize: "cover", height: 13+"rem"}}>
          
              <h3>{item.title}</h3>
              </div>
              </Link>
         
           
          
         
          )}
        </div>
        </div>
        </main>);}}
        
      
        
 
export default Items;