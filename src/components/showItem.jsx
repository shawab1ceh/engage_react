import React,{Component} from 'react';

import Header from './header'



class ShowItem extends Component {
    state = {
        item:[]
    }

    componentDidMount() {

        //  this.setState({items:getItems()})
        
        fetch('http://local.engageapi:8080/api/showItem/'+this.props.name)
          .then(res => res.json())
          .then((response) => {
            this.setState({ item: response })
          })
          .catch(console.log("error"))
        }
       
    
    
    
    render() { 
        const {item}=this.state;
        return ( 
        <React.Fragment>
            <Header/>
            <h1 style={{textAlign:"center"}}>{item.title}</h1>
            <img src={item.image} alt="" style={{marginLeft:"34%"}}/>
            </React.Fragment> 
       
            );
    }
}
 
export default ShowItem;
