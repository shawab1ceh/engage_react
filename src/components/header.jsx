import React from 'react';

const Header = () => {
    return ( 
        <header>
        <div className="headerDecorations">
          <h1>
            {/* <i
              style={{marginRight:0.5+"rem"}}
              className="fa fa-handshake-o"
              aria-hidden="true"
            ></i> */}
            Dramatically Engage
          </h1>
          <p>
            Objectively innovative empowered manufactured products whereas
            parallel platforms
          </p>
          <button>Engage Now</button>
        </div>
      </header> );
}
 
export default Header;

 