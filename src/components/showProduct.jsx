import React,{Component} from 'react';

import Header from './header'



class ShowProduct extends Component {
    state = {
        product:[]
    }

    componentDidMount() {
         fetch('http://local.engageapi:8080/api/showProduct/'+this.props.name)
        .then(res => res.json())
        .then((response) => {
          this.setState({ product: response })
        })
        .catch(console.log)
      }
    
    render() { 
       const {product}=this.state;
        return ( 
        <React.Fragment>
            <Header/>
            <h1 style={{textAlign:"center",paddingTop:3+"rem"}}>{product.title}</h1>
            <img src={product.image} alt="" style={{paddingLeft:"44.315%"}}/>
            <p style={{textAlign:"center",paddingTop:"10px"}}>{product.body}</p>
            </React.Fragment> );
    }
}
 
export default ShowProduct;
