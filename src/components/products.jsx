import React, { Component } from 'react';
// import {getProducts} from '../services/products';
import {Link} from "react-router-dom"
class Products extends Component {
    state = { 
        products:[]
     }
    componentDidMount() {

    //   this.setState({products:getProducts()})

    fetch('http://local.engageapi:8080/api/getProducts')
        .then(res => res.json())
        .then((response) => {
          this.setState({ products: response })
        })
        .catch(console.log)
      }
    
      
    
    render() { 
        return( 
            <main>
                <h2>World Class Products</h2>
                <div className="products">
            {this.state.products.map(product=>
            <div className="product" key={product.id}>
                <img src={product.image} alt=""/>
                <div className="product-body">
                <h5><Link to={`products/${product.id}`}>{product.title}</Link></h5>
                <p>{product.body.substring(0,20)+"..."}</p>
                <Link to={`products/${product.id}`}>read more</Link>
                </div>
                </div>
                )}
                </div>
            </main>
        );
    }}
       
export default Products;