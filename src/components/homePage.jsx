import React from 'react';

import Items from './items'
import Header from './header'
import Products from './products'



const HomePage=()=>{
  return (
   <React.Fragment>
  <Header/>
  <Items/>
  <Products/>
  </React.Fragment>
 
  );
}

export default HomePage;