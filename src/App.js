import React from 'react';
import ShowItem from './components/showItem'
import ShowProduct from './components/showProduct'
import HomePage from './components/homePage'
import {Route,Switch} from "react-router-dom"

import './App.css';

function App() {
  return (
  <Switch>
  <Route exact path="/" component={HomePage}/>
  <Route exact path="/items/:name" 
   render={routeProps=><ShowItem name={routeProps.match.params.name}/>}/>
  <Route exact path="/products/:name" 
   render={routeProps=><ShowProduct name={routeProps.match.params.name}/>}/>
  <Route render={()=><h1 style={{textAlign:"center",paddingTop:25+"rem"}}>Error 404 !!! Page not found</h1>}/>
  </Switch>
  
  );
}

export default App;
